Current state of WHMCS API module Drupal 7
------------------------------------------
This module provides the basic connection between Drupal and WHMCS API, 
it is required by other WHMCS Drupal integration modules.

Installation
------------
1.Upload and extract the files under /sites/all/modules 
of your Drupal installation, like any other module.
2.Enable the module at Administration » Modules.
3.Create a new WHMCS administrator role with only API access enabled from 
"Setup » Staff Manager » Administrator Roles" and a new user with that role from 
"Setup » Staff Manager » Administrator Users" according to WHMCS Documentation.
4.You will need to enable WHMCS AutoAuth access as well for WHMCS AUTH module 
by adding a $autoauthkey variable in configuration.php located under 
your WHMCS installation according to this: http://docs.whmcs.com/AutoAuth.
5.Then you will need to configure administrator settings 
under:/admin/config/whmcs/api with required information 
(WHMCS installation path, WHMCS API user/pass, redirect pages).


Basic usage
-----------
None. It just enables other modules like (WHMCS Authentication module) 
to use WHMCS API.



Maintainers
-----------
- n.stefanis (Nicolaos Stefanis)
- ploupas (Panagiotis Loupas)
